//
//  ContainerViewController.h
//  SplitViewTest
//
//  Created by Sam Suthar on 03/05/18.
//  Copyright © 2018 Sandeep Suthar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SplitViewController.h"
@interface ContainerViewController : UIViewController
@property(nonatomic) SplitViewController *splitViewController;
@end
