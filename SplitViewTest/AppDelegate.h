//
//  AppDelegate.h
//  SplitViewTest
//
//  Created by Sam Suthar on 02/05/18.
//  Copyright © 2018 Sandeep Suthar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

