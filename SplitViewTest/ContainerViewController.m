//
//  ContainerViewController.m
//  SplitViewTest
//
//  Created by Sam Suthar on 03/05/18.
//  Copyright © 2018 Sandeep Suthar. All rights reserved.
//

#import "ContainerViewController.h"
#import "EmptyViewController.h"
@interface ContainerViewController ()<UISplitViewControllerDelegate>

@end

@implementation ContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"splitVC"]){
        _splitViewController = (SplitViewController *)segue.destinationViewController;
        _splitViewController.delegate = self;
        _splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModeAllVisible;
    }
}

#pragma -mark
#pragma -mark Override TraitCollection
-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
   /* for (UIViewController *vc in self.childViewControllers) {
        [self setOverrideTraitCollection:[UITraitCollection traitCollectionWithHorizontalSizeClass:UIUserInterfaceSizeClassCompact] forChildViewController:vc];
    }*/
}

#pragma -mark
#pragma -mark UISplitViewControllerDelegate
- (BOOL)splitViewController:(UISplitViewController *)splitViewController collapseSecondaryViewController:(UIViewController *)secondaryViewController ontoPrimaryViewController:(UIViewController *)primaryViewController{
    if ([secondaryViewController isKindOfClass: [UINavigationController class]]) {
        UIViewController *vc  =  ((UINavigationController *)secondaryViewController).viewControllers.firstObject;
        if ([vc isKindOfClass:[EmptyViewController class]]) {
            return YES;
        }else if (vc.navigationItem.leftBarButtonItem != nil){
            // Show Defualt Navigation Back Button
            vc.navigationItem.leftBarButtonItem = nil;
        }
    }
    return NO;
}
- (UIViewController *)splitViewController:(UISplitViewController *)splitViewController separateSecondaryViewControllerFromPrimaryViewController:(UIViewController *)primaryViewController{
    if ([self isDisplayingDetailSceneWithPrimaryViewController:primaryViewController]){
        // Keep the detail scene
        return nil;
    }
    // Replace the detail scene with the "empty" scene
    return [self emptySelectionNavigationController];;
}
- (BOOL)isDisplayingDetailSceneWithPrimaryViewController:(UIViewController *)primaryViewController{
    if ([primaryViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navController = (UINavigationController *)primaryViewController;
        if (navController.viewControllers.count > 1) {
            UINavigationController *detailNavController = (UINavigationController *)navController.viewControllers[1];
            UIViewController * visibleDetailViewController = detailNavController.viewControllers.firstObject;
            
            if (![visibleDetailViewController isKindOfClass:[EmptyViewController class]]) {
                // Add Close Button to Show Empty View
                if (![visibleDetailViewController.navigationItem.leftBarButtonItem respondsToSelector:@selector(dismissVC:)]) {
                    visibleDetailViewController.navigationItem.leftBarButtonItem = [self getCloseButton];
                }
                return true;
            }
        }
    }
    return false;
}
- (BOOL)splitViewController:(UISplitViewController *)splitViewController showDetailViewController:(UINavigationController *)navController sender:(nullable id)sender{
    if (navController.viewControllers.count>0) {
        UIViewController *vc = navController.viewControllers.firstObject;
        if (![vc isKindOfClass:[EmptyViewController class]] && !splitViewController.isCollapsed) {
            vc.navigationItem.leftBarButtonItem = [self getCloseButton];
        }
    }
    return  NO;
}
-(void)dismissVC:(id)sender{
    [_splitViewController showDetailViewController:[self emptySelectionNavigationController] sender:self] ;
}
-(UINavigationController *)emptySelectionNavigationController{
    static UINavigationController *nav;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"EmptyViewController"];
        nav= [[UINavigationController alloc]initWithRootViewController:vc];
    });
    return nav;
}
-(UIBarButtonItem *)getCloseButton{
    return [[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(dismissVC:)];
}


@end
