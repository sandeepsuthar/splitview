//
//  DetailViewController.m
//  SplitViewTest
//
//  Created by Sam Suthar on 02/05/18.
//  Copyright © 2018 Sandeep Suthar. All rights reserved.
//

#import "DetailViewController.h"
#import "AnotherViewController.h"
@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHue:drand48() saturation:1.0 brightness:1.0 alpha:1.0];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pushViewController:(id)sender {
    AnotherViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"AnotherViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    /*UINavigationController *nav  = [[UINavigationController alloc]initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:nav animated:YES completion:nil];*/
}
@end
